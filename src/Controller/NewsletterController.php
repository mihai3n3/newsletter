<?php

namespace App\Controller;

use App\Entity\Newsletter;
use App\Repository\NewsletterRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class NewsletterController extends Controller
{
    /**
     * @Route("/", name="newsletter")
     */
    public function index(): Response
    {
        return $this->render('newsletter/index.html.twig');
    }

    /**
     * @Route("/subscribe", name="newsletter_subscribe")
     */
    public function subscribe(Request $request, NewsletterRepository $newsletterRepository, ValidatorInterface $validator): JsonResponse
    {
        $email = $request->get('email');

        if (!$request->isXmlHttpRequest()) {
            return $this->json([
                'error' => true,
                'message' => ['Not an ajax'],
            ]);
        }

        if (empty($email)) {
            return $this->json([
                'error' => true,
                'message' => 'Email can not be empty !',
            ], Response::HTTP_FORBIDDEN);
        }

        $emailConstraint = new EmailConstraint();
        /** @var ConstraintViolationList $errors */
        $errors = $validator->validate($email, $emailConstraint);
        if (count($errors) > 0) {
            $errorsString = [];
            foreach ($errors as $error) {
                $errorsString[] = $error->getMessage();
            }

            return $this->json([
                'error' => true,
                'message' => $errorsString,
            ], Response::HTTP_FORBIDDEN);
        }

        $newsletter = $newsletterRepository->findOneBy(['email' => $email]);

        if ($newsletter instanceof Newsletter) {
            dump($newsletter);
            return $this->json([
                'error' => true,
                'message' => ['Email already subscribed !'],
            ], Response::HTTP_FORBIDDEN);
        }

        try {
            $em = $this->getDoctrine()->getManager();

            $newsletter = new Newsletter();
            $newsletter->setEmail($email);
            $em->persist($newsletter);
            $em->flush();
        } catch (Exception $e) {
            return $this->json([
                'error' => true,
                'message' => $e->getMessage(),
            ], Response::HTTP_FORBIDDEN);
        }

        return $this->json([
            'error' => false,
            'message' => 'Email added with success !',
        ]);
    }
}